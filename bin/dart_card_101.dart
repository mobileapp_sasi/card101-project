import 'dart:io';
import 'dart:math';

import 'package:dart_card_101/dart_card_101.dart' as dart_card_101;

void main() {
  Game game = Game();
  game.showWelcome();
  game.askName();
  game.addOnhandCard();
  game.shuffleDeck();
  while (true) {
    game.showTurn();
    game.showOnhand();
    game.askDisCard();
    game.showCountPoint();
    game.switchPlayer();
    if (game.checkLose()) {
      game.showWin();
      break;
    }
  }
}

class Game {
  Player p1 = Player();
  Player p2 = Player();
  Player currentPlayer = Player();
  Deck hitDeck = Deck();
  int countPoint = 0;
  bool lose = false;

  Game() {
    currentPlayer = p1;
  }

  void showWelcome() {
    print('Welcome To Card101 Game!!');
  }

  void askName() {
    if (currentPlayer == p1) {
      print('Please Input Name Of Player 1 :');
      String inputName = stdin.readLineSync()!;
      setPlayerName(inputName);
      switchPlayer();
      askName();
    } else {
      print('Please Input Name Of Player 2 :');
      String inputName = stdin.readLineSync()!;
      setPlayerName(inputName);
      switchPlayer();
    }
  }

  void setPlayerName(String name) {
    if (currentPlayer == p1) {
      p1.setName(name);
    } else {
      p2.setName(name);
    }
  }

  void addOnhandCard() {
    for (int i = 0; i < 3; i++) {
      p1.addCard(hitDeck.deal());
      p2.addCard(hitDeck.deal());
    }
  }

  Deck shuffleDeck() {
    hitDeck.shuffle();
    return hitDeck;
  }

  void showTurn() {
    print("${currentPlayer.getName()}'s Turn");
  }

  void showOnhand() {
    currentPlayer.showHand();
  }

  int getCountPoint() {
    return countPoint;
  }

  void showCountPoint() {
    print('\n Current Count Point is : ${getCountPoint()} \n');
  }

  void askDisCard() {
    print('Please input number of card that you want to discard');
    int disCard = int.parse(stdin.readLineSync()!);
    inputDisCard(disCard);
  }

  void inputDisCard(int num) {
    countPoint += currentPlayer.disCard(num);
    currentPlayer.addCardAtIndex(num, hitDeck.deal());
  }

  void switchPlayer() {
    if (currentPlayer == p1) {
      currentPlayer = p2;
    } else {
      currentPlayer = p1;
    }
  }

  Player getCurrentPlayer() {
    return currentPlayer;
  }

  bool isLose() {
    return lose;
  }

  bool checkLose() {
    if (countPoint > 101) {
      return lose = true;
    } else {
      return lose = false;
    }
  }

  void showWin() {
    print('You Lose!!');
    print('\n Winner is ${currentPlayer.getName()} !!!');
  }
}

class Player {
  String name = "";
  List onhand = [];
  int totalOnhand = 0;

  Player() {}

  String getName() {
    return name;
  }

  List getOnhand() {
    return onhand;
  }

  int getTotalOnhand() {
    return totalOnhand;
  }

  void setName(String playerName) {
    name = playerName;
  }

  void addCard(Card card) {
    if (totalOnhand >= 3) {
      return print('Total Onhand Is Full');
    }
    onhand.length++;
    onhand[totalOnhand] = card;
    totalOnhand++;
  }

  void addCardAtIndex(int index, Card card) {
    onhand[index - 1] = card;
  }

  int disCard(int num) {
    if (totalOnhand < 3) {
      print('Total Onhand Is Must Be 3 Card');
      return 0;
    }
    Card temp = onhand[num - 1];
    return temp.getCardValue();
  }

  void showHand() {
    print("$name's card");
    for (int i = 0; i < totalOnhand; i++) {
      print('${i + 1} : ${onhand[i]}');
    }
  }
}

class Deck {
  List listCard = [];
  int totalCard = 52;

  Deck() {
    int num = 0;
    for (int i = 0; i <= 4; i++) {
      for (int j = 1; j <= 13; j++) {
        listCard.length++;
        listCard[num] = Card(i, j);
        num++;
      }
    }
  }

  void showDeck() {
    for (int i = 0; i < totalCard; i++) {
      print(listCard[i]);
    }
  }

  void shuffle() {
    Random random = Random();

    Card temp;

    int j;
    for (int i = 0; i < totalCard; i++) {
      j = random.nextInt(totalCard);
      temp = listCard[i];
      listCard[i] = listCard[j];
      listCard[j] = temp;
    }
  }

  Card deal() {
    Card top = listCard[0];

    for (int i = 1; i < totalCard; i++) {
      listCard[i - 1] = listCard[i];
    }
    listCard[totalCard - 1] = null;
    totalCard--;
    return top;
  }
}

class Card {
  int cardSuit;
  int cardFace;
  // int cardValue;

  Card(this.cardSuit, this.cardFace);

  int getCardSuit() {
    return cardSuit;
  }

  int getCardFace() {
    return cardFace;
  }

  int getCardValue() {
    return cardFace;
  }

  @override
  String toString() {
    String str = "Error";

    switch (cardFace) {
      case 1:
        str = "Ace";
        break;

      case 2:
        str = "Two";
        break;

      case 3:
        str = "Three";
        break;

      case 4:
        str = "Four";
        break;

      case 5:
        str = "Five";
        break;

      case 6:
        str = "Six";
        break;

      case 7:
        str = "Seven";
        break;

      case 8:
        str = "Eight";
        break;

      case 9:
        str = "Nine";
        break;

      case 10:
        str = "Ten";
        break;

      case 11:
        str = "Jack";
        break;

      case 12:
        str = "Queen";
        break;

      case 13:
        str = "King";
        break;
    }
    return "$str of ${suitToString(cardSuit)}";
  }

  String suitToString(int num) {
    String str = "Error";
    switch (num) {
      case 0:
        str = "Spades";
        break;
      case 1:
        str = "Hearts";
        break;
      case 2:
        str = "Clubs";
        break;
      case 3:
        str = "Diamonds";
        break;
    }
    return str;
  }
}
